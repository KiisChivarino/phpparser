<?php
require_once '../lib/VirtueMartParser.php';

$vmParser = new VirtueMartParser();
//require_once '../phpQuery-onefile.php';
//header('Content-Type: text/html; charset=Windows-1251');

$links_array = $vmParser->getLinks('http://www.bravomebel.ru', 'http://www.bravomebel.ru/catalog/68.html', '.catalog_folder');//выбираю в массив ссылки со страницы "мебель из массива"
//var_dump($swf_links_array);
$pages_array = [];

foreach($links_array as $link){
	$html = file_get_contents($link);
	phpQuery::newDocument($html); //solid wood furniture - мебель из массива
	$title = pq('h1')->text();
	$img_href = pq('.zoom_picture')->attr('href');
	$vmParser->putImg('http://www.bravomebel.ru'.$img_href);
	$img = substr($img_href,strrpos($img_href,'/')+1);
	$imgs = array($img);
	/*if(empty(pq('td[width=100%][valign=top] p:last-child')->text()))
		pq('td[width=100%][valign=top]')->remove();
	else
		pq('td[width=100%][valign=top] p:last-child')->remove();
	*/
	pq('td[width=100%][valign=top] .zoom_picture')->remove();
	
	pq('td[width=100%][valign=top] font')->attr('color','');
	
	pq('td[width=100%][valign=top] table tbody tr:first-child')->remove();
	
	$desc = iconv("Windows-1251", "UTF-8", pq('td[width=100%][valign=top]')->html());
	
	$pages_array[] = array(
						'product_sku'=>'no_sku'.rand(1,10000),
						'category_path'=>'Мебель из массива/Малогабаритные кровати',
						'manufacturer_name'=>'Браво мебель',
						'product_name'=>$title,
						'product_desc'=>$desc,
						'file_urls'=> $imgs,
						'published'=>'1'
					 );
	
	phpQuery::unloadDocuments($swf);
}

$vmParser->createGoodsXML($pages_array);