<?php
require_once '../lib/VirtueMartParser.php';

$vmParser = new VirtueMartParser();
$vmParser->domen = 'http://lerom-mebel.ru';
$vmParser->path = '/show_cat2.php?grid=4';
$pages_array = [];
$manufacturer_id = '9';
$category_id = '71';

$html = iconv('Windows-1251', 'UTF-8', file_get_contents($vmParser->domen . $vmParser->path)); //страница раздела

$pqCategory = phpQuery::newDocument($html);

$links_arr = $vmParser->getLinks('table[width="98%"] a.tov');

foreach($links_arr as $key=>$link){
	if(strlen($link)>27) unset($links_arr[$key]);
	if(strpos($link,'show_cat')!==false) unset($links_arr[$key]);
}
phpQuery::unloadDocuments($pqCategory);

foreach($links_arr as $link){
	$vmParser->path = '/'.$link;
	$html = iconv('Windows-1251', 'UTF-8', file_get_contents($vmParser->domen . $vmParser->path)); //страница раздела
	$pqProduct = phpQuery::newDocument($html);
	
	$title = pq('font[size=4]')->text();
	if ($title == 'Композиции Мелисса' or $title == 'Композиции Оливия' or $title == 'Композиции Патриция'){
		$descTable1 = pq('table.textmenu>tr:nth-child(5)>td:nth-child(1)>table:nth-child(1)');
		$descTable2 = pq('table.textmenu>tr:nth-child(5)>td:nth-child(1)>table:nth-child(2)');
	}else{
		$descTable1 = pq('table.textmenu>tr:nth-child(7)>td:nth-child(1)>table:nth-child(1)');
		$descTable2 = pq('table.textmenu>tr:nth-child(7)>td:nth-child(1)>table:nth-child(2)');
	}
	
	$color = $descTable1->find('tr:nth-child(1)>td:nth-child(2)')->text();
	$facade = $descTable1->find('tr:nth-child(2)>td:nth-child(2)')->text();
	$equipment = $descTable1->find('tr:nth-child(3)>td:nth-child(2)')->text();
	$description = $descTable2->find('tr:nth-child(1)>td:nth-child(1)')->html();
	
	$desc = '<div>';
	$desc .='<b>Цвет:</b> '.$color.'<br/>';
	$desc .='<b>Фасад:</b> '.$facade.'<br/>';
	$desc .='<b>Комплектация:</b> '.$equipment.'<br/>';
	$desc .=$description;
	$desc .= '</div>';
	
	$img_arr = [];
	if($title == 'Композиции Мелисса' or $title == 'Композиции Оливия' or $title == 'Композиции Патриция'){
		$imgString = pq('table.textmenu>tr:nth-child(3)>td:nth-child(1)>img')->attr('src');
		$imgName = $vmParser->putImg($imgString);
		if($imgName) $img_arr[] = $imgName;
	}else{
		$imgs = pq('table.textmenu>tr:nth-child(5)>td:nth-child(1) a');
			foreach ($imgs as $img){
				//var_dump(pq($img)->attr('href'));
				$imgString = pq($img)->attr('href');
				$imgName = $vmParser->putImg($imgString,"r=", "',");
				if($imgName) $img_arr[] = $imgName;
			}
	}
	//готовлю массив для формирования xml
	$pages_array[] = array(
							'product_sku'=>$vmParser->getRandArt(),
							'category_id'=>$category_id,
							'manufacturer_id'=>$manufacturer_id,
							'product_name'=>$title,
							'product_desc'=>$desc,
							'file_urls'=> $img_arr,
							'published'=>'1',
							//'product_parent_id'=>$product_parent_id
						);
	
	phpQuery::unloadDocuments($pqProduct);
}
//$vmParser->createGoodsXML($pages_array); //формирую xml
$vmParser->createGoodsXMLNew($pages_array);
