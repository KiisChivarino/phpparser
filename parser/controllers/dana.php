<?php
require_once '../lib/VirtueMartParser.php';
ini_set('max_execution_time', 0);
//header('Content-Type: text/html; charset=windows-1251');
$vmParser = new VirtueMartParser();

$product_parent_id = '3154';
$link = 'http://www.dana.ru/catalog/409/?SHOWALL_1=1';
$links_array = $vmParser->getLinks('http://www.dana.ru', $link, '.catalog-section__item a');//выбираю в массив ссылки для гостиной

$pages_array = [];

foreach($links_array as $lnk){
	//sleep(1);
		usleep(500000);
		$html = file_get_contents($lnk);
		phpQuery::newDocument($html); //solid wood furniture - мебель из массива
		
		$title = pq('h1')->text();
		
		/*↓выбираю имена картинок в массив, сохраняю файлы картинок↓*/
		$img_hrefs = pq('.item__carousel a');
		$imgs = [];
		foreach($img_hrefs as $href){
			if(pq($href)->attr('href')!=='#')
				$img = pq($href)->attr('href');
			else continue;
				$vmParser->imageresize('../data/imgs/'.substr('http://www.dana.ru'.$img,strrpos('http://www.dana.ru'.$img,'/')+1),'http://www.dana.ru'.$img,600,600,75);
				//$vmParser->putImg('http://www.dana.ru'.$img);
				$imgs[] = substr($img,strrpos($img,'/')+1);
		}
		/*↑выбираю имена картинок в массив, сохраняю файлы картинок↑*/


		$desc = '';
		$desc = pq('.item__descr')->html();
		pq('.item__text b')->remove();
		pq('.item__text a')->remove();
		pq('.item__text p')->remove();
		$desc = '<p>'.$desc. pq('.item__text')->html().'</p>';
		$desc = $desc. '<p><strong>Габаритные размеры:</strong> ' .pq('.item__parms li:nth-child(2)')->html() . '</p>';
		
		$pages_array[] = array(
							'product_sku'=>'no_sku'.rand(1,10000),
							//'category_path'=>'Гостиные',
							'manufacturer_name'=>'Дана',
							'product_name'=>$title,
							'product_desc'=>$desc,
							'file_urls'=> $imgs,
							'published'=>'1',
							'product_parent_id'=>$product_parent_id
						);
		phpQuery::unloadDocuments();
}
$vmParser->createGoodsXML($pages_array);






/*↓ Выборка всех товаров раздела ↓*/
/*
$links_array = $vmParser->getLinks('http://www.dana.ru', 'http://www.dana.ru/catalog/393/', 'a[style="font-size: 13px;"]');//выбираю в массив ссылки со страницы "мебель из массива"
//$links_array = $vmParser->getLinks('http://www.dana.ru', 'http://www.dana.ru/catalog/395/', '.catalog-section__item a');//выбираю в массив ссылки со страницы "мебель из массива"

//var_dump($links_array);

$links_num_page_arr = [];

$pages_array = [];

$sub_links_array = [];
foreach($links_array as $link){
	//sleep(1);
	usleep(500000);
	
	$html = file_get_contents($link);
	$sub_links_array = array_merge ($sub_links_array, $vmParser->getLinks('http://www.dana.ru', $link.'?SHOWALL_1=1', '.catalog-section__item a'));

}
foreach($sub_links_array as $lnk){
		//sleep(1);
		usleep(500000);
		$html = file_get_contents($lnk);
		phpQuery::newDocument($html); //solid wood furniture - мебель из массива
		
		$title = pq('h1')->text();
		
		$img_href = pq('.item__carousel a')->attr('href');
		$vmParser->putImg('http://www.dana.ru'.$img_href);
		$img = substr($img_href,strrpos($img_href,'/')+1);
		$imgs = array($img);
		
		$desc = '';
		$desc = pq('.item__descr')->html();
		pq('.item__text b')->remove();
		pq('.item__text a')->remove();
		pq('.item__text p')->remove();
		$desc = '<p>'.$desc. pq('.item__text')->html().'</p>';
		$desc = $desc. '<p><strong>Габаритные размеры:</strong> ' .pq('.item__parms li:nth-child(2)')->html() . '</p>';
		
		$pages_array[] = array(
							'product_sku'=>'no_sku'.rand(1,10000),
							'category_path'=>'Гостиные',
							'manufacturer_name'=>'Дана',
							'product_name'=>$title,
							'product_desc'=>$desc,
							'file_urls'=> $imgs,
							'published'=>'1'
						);
		phpQuery::unloadDocuments();
}
$vmParser->createGoodsXML($pages_array);
//var_dump($pages_array);
*/
/*↑ Выборка всех товаров раздела ↑*/