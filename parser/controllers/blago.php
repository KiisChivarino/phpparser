<?php
require_once '../lib/Parser.php';

$ocParser = new OpenCartParser();
$ocParser->domen = 'http://blago-mebel.ru';
$ocParser->path = '/catalog/prihojie/kojanye/';
$pages_array = [];

$html = file_get_contents($ocParser->domen . $ocParser->path); //страница раздела

$cat_links = $ocParser->getLinks('.cat_section_list .link');

$pages_array = [];

foreach ($cat_links as $link){
    
    $html = file_get_contents($ocParser->domen . $link);

    $pqMainProduct = phpQuery::newDocument($html);
        //вытаскиваю данные
        $_NAME_ = trim(pq('h1')->text());
        $_CATEGORY_ = 'Прихожие';
        $_SKU_ = trim(pq(pq('.properties>.row:first-child')[0])->find('.col-xs-6:last-child')->text());
        $_MODEL_ = $_SKU_;
        $_MANUFACTURER_ = 'Благо';
        $_PRICE_ = '';
        $_DESCRIPTION_ = '';
        
        
        //формирую характеристики
        $i = 0;
        $featuresArr[$i]['f_key'] = 'Габариты';
        $featuresArr[$i]['f_val'] = trim(pq(pq('.properties>.row:nth-child(2)')[0])->find('.col-xs-6:last-child')->text());
        $i++;
        $featuresArr[$i]['f_key'] = 'Материал';
        $featuresArr[$i]['f_val'] = trim(pq(pq('.properties>.row:nth-child(3)')[0])->find('.col-xs-6:last-child')->text());
        $i++;
        $featuresArr[$i]['f_key'] = 'Цвет';
        $featuresArr[$i]['f_val'] = trim(pq(pq('.properties>.row:nth-child(4)')[0])->find('.col-xs-6:last-child')->text());
        
        $_ATTRIBUTES_ = $ocParser->getFeaturesString($featuresArr);
        
        $_IMAGE_ = pq(pq('prod_big_img')[0])->text();
        
        $_IMAGES_ = array(pq(pq('prod_big_img')[0])->text());
        
        $pages_array[] = array(
							'_NAME_'=>$_NAME_,
							'_CATEGORY_'=>$_CATEGORY_,
							'_SKU_'=>$_SKU_,
							'_MODEL_'=>$_MODEL_,
							'_MANUFACTURER_'=>$_MANUFACTURER_,
                            '_PRICE_'=>$_PRICE_,
						);
        
    phpQuery::unloadDocuments($pqMainProduct);

}
