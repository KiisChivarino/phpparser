<?php
require_once '../lib/VirtueMartParser.php';
ini_set('max_execution_time', 0);
$vmParser = new VirtueMartParser();
$vmParser->domen = 'http://lerom-mebel.ru';
$vmParser->path = '/show_cat2.php?grid=4';
$vmParser->dbh = new PDO('mysql:host=localhost;dbname=telfs_7slonov', 'telfs_7slonov', '7slonov7');
$manufacturer_id = '9';
$category_id = '71';

$ids_arr = $vmParser->getParentIds($manufacturer_id,$category_id);
//var_dump($ids_arr);
$html = iconv('Windows-1251', 'UTF-8', file_get_contents($vmParser->domen . $vmParser->path)); //страница раздела
$pages_array = [];

$pqCategory = phpQuery::newDocument($html);

$links_arr = $vmParser->getLinks('table[width="98%"] a.tov');

foreach($links_arr as $key=>$link){
	if(strlen($link)>27) unset($links_arr[$key]);
	if(strpos($link,'show_cat')!==false) unset($links_arr[$key]);
}

phpQuery::unloadDocuments($pqCategory);

foreach($links_arr as $link){
	$vmParser->path = '/'.$link;
	$html = iconv('Windows-1251', 'UTF-8', file_get_contents($vmParser->domen . $vmParser->path)); //страница раздела
	
	$pqProduct = phpQuery::newDocument($html);
	$title = pq('font[size=4]')->text();
	$product_parent_id = '';
	
	foreach($ids_arr as $idArr){
		if(trim($idArr['product_name'])==trim($title)) {$product_parent_id = $idArr['virtuemart_product_id']; break;}
	}
	pq('body > table:nth-child(2) > tr > td:nth-child(2) > table > tr > td:nth-child(1) > table > tr > td > table > tr:nth-child(2) > td:nth-child(2) > table > tr:nth-child(7) > td > table>tr:first-child')->remove();
	$elementsContainer = pq('body > table:nth-child(2) > tr > td:nth-child(2) > table > tr > td:nth-child(1) > table > tr > td > table > tr:nth-child(2) > td:nth-child(2) > table > tr:nth-child(7) > td > table > tr:nth-child(2n-1)');
	foreach ($elementsContainer as $element){
		$imgName = '';
		$imgString = pq($element)->find('td:first-child>a:last-child')->attr('href');
		if(trim($imgString)) $imgName = $vmParser->putImg($imgString,"r=", "',", true);
		$img_arr = array($imgName);
		$child_title = pq($element)->find('td:nth-child(2)>table>tr:first-child')->text();
		$child_type = pq($element)->find('td:nth-child(2)>table>tr:nth-child(2)')->html();
		//$child_title = $child_type.$child_title;
		$child_dimensions = pq($element)->find('td:nth-child(3)')->text();
		$desc = '<div>';
		$desc .= $child_type . '<br/>';
		if(!empty($child_dimensions))$desc .= '<b>Габариты: </b>' . $child_dimensions . '<br/>';
		$desc .= '</div>';
		
		//готовлю массив для формирования xml
		$pages_array[] = array(
							'product_sku'=>$vmParser->getRand('no_sku_', count($pages_array)),
							//'category_path'=>'Гостиные',
							'manufacturer_id'=>$manufacturer_id,
							'product_name'=>$child_title,
							'product_desc'=>$desc,
							'published'=>'1',
							'product_parent_id'=>$product_parent_id,
							'file_urls'=> $img_arr,
						);
	}
	phpQuery::unloadDocuments($pqProduct);
}
$vmParser->createGoodsXMLNew($pages_array);