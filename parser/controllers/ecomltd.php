<?php
require_once '../lib/VirtueMartParser.php';

$vmParser = new VirtueMartParser();
$domen = 'http://www.ecomltd.ru';
$pages_array = [];


$html = iconv('Windows-1251', 'UTF-8', file_get_contents($domen.'/katalog/shkafy_kupe_dvuhdvernye/shkaf_kupe_dvuhdvernyj_tivoli_153524004564_mm/?lang=ru')); //страница раздела

$pqGoods = phpQuery::newDocument($html);

$goods_array = pq('div[style=padding-left:10px;padding-right:10px;]'); //карточки товаров на странице раздела

foreach ($goods_array as $product){
	
	//формирую название товара
	$title = pq($product)->find('span[style=color:#946244; font-weight:bold]')->text();
	if(strpos($title,'1535*2400*640'))continue;
	if(strpos($title,'/')) $title = substr($title,0, strpos($title,'/')); //убираю 'гл.45 см' со слэшами
	$title = str_replace ('гл.45 см', '', $title);
	
	$link = $domen.pq($product)->find('a')->attr('href'); //ссылка на страницу товара
	
	$linkHtml = iconv('Windows-1251', 'UTF-8', file_get_contents($link));
	
	$pqLink = phpQuery::newDocument($linkHtml);
	
	/*↓вытаскиваю артикул, картинки и описание из страницы товара↓*/
	$product_sku = pq('b:contains("Артикул")')->elements[0]->nextSibling->nodeValue; 
	
	$color = pq('b:contains("Цвет")')->elements[0]->nextSibling->nodeValue;	
	$size =  pq('b:contains("Габариты (Д*Ш*В)")')->elements[0]->nextSibling->nodeValue;
	if(strpos($size,'172*45*450')) $size = $size.' | 1535*2400*640'; //вместо двух товаров делаю один с разными габаритами
	$material =  pq('b:contains("Материал")')->elements[0]->nextSibling->nodeValue;
	$manufacturer =  pq('b:contains("Производитель")')->elements[0]->nextSibling->nodeValue;
	
	$description =  pq('b:contains("Описание")')->elements[0]->nextSibling->nodeValue;
	if(empty(trim($description))) $description = pq('b:contains("Описание")')->elements[0]->nextSibling->nextSibling->nodeValue;
	$description = '<p>'.$description.'</p>';
	
	$volume =  pq('b:contains("Объем")')->elements[0]->nextSibling->nodeValue;
	$weight =  pq('b:contains("Вес")')->elements[0]->nextSibling->nodeValue;
	
	
	$imgs = pq('div>center>img');
	$img_arr = [];
	foreach ($imgs as $img){
		if(!empty(pq($img)->attr('onclick'))) {
			$imgString = pq($img)->attr('onclick');
			$imgHref = $domen.substr($imgString, strpos($imgString,"('")+2, strpos($imgString, "',") - strpos($imgString,"('")-2); //вытаскиваю ссылку из параметров функции
			$imgName = rand(1,10000).substr($imgHref,strrpos($imgHref,'/')+1); //вытаскиваю имя картинки, добавляю случайное число
			$vmParser->imageresize('../data/imgs/'.$imgName,$imgHref,600,600,75); //ограничиваю размер не более 600px и качество
			$img_arr[] = $imgName;
		}
	}
	
	$desc = '<div>';	
	$desc .= '<b>Цвет:</b>'.$color.'<br />';
	$desc .= '<b>Габариты (Д*Ш*В):</b>'.$size.'<br />';
	$desc .= '<b>Материал:</b>'.$material.'<br />';
	$desc .= '<b>Производитель:</b>'.$manufacturer.'<br />';
	$desc .= '<b>Описание:</b><br />'.$description;
	$desc .= '<b>Объем:</b>'.$volume.'<br />';
	$desc .= '<b>Вес:</b>'.$weight.'<br />';
	$desc .= '</div>';
	/*↑вытаскиваю артикул,картинки и описание из страницы товара↑*/
	
	phpQuery::unloadDocuments($pqLink);
	
	//готовлю массив для формирования xml
	$pages_array[] = array(
							'product_sku'=>$product_sku,
							'category_path'=>'Шкафы - купе',
							'manufacturer_name'=>'Лидер-М',
							'product_name'=>$title,
							'product_desc'=>$desc,
							'file_urls'=> $img_arr,
							'published'=>'1',
							//'product_parent_id'=>$product_parent_id
						);
	
}

phpQuery::unloadDocuments($pqGoods);

$vmParser->createGoodsXML($pages_array); //формирую xml