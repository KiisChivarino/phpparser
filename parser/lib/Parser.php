<?php
require 'phpQuery-onefile.php';

class Parser{
    private $imgFtpUrl = '/images/stories/virtuemart/product/';
	private $dataFileUrl = '../data/tables/goods.xml';
	public $domen = '';
	public $path = '';
	public $dbh;
    public function __construct(){
		
	}
    public function getLinks($selector){
		$html = file_get_contents($this->domen.$this->path);
		$links_array = [];
		
		phpQuery::newDocument($html); //solid wood furniture - мебель из массива
		
		$links_obj = pq($selector);
		
		foreach($links_obj as $link){
			$href = pq($link)->attr('href');
			$links_array[] = $href;
		}
		
		phpQuery::unloadDocuments();
		
		return array_unique($links_array);
	}
    
    public function putImg($imgString, $lBorderStr=null, $rBorderStr=null, $copyImg=true){
		
		if($lBorderStr!==null and $rBorderStr!==null){
			$subStrStart = strpos($imgString,$lBorderStr)+strlen($lBorderStr);
			$subStrLength = strpos($imgString, $rBorderStr) - strpos($imgString,$lBorderStr)-strlen($lBorderStr);
			$imgHref = substr($imgString, $subStrStart, $subStrLength);
		}else{
			$imgHref = $imgString;
		}
		if(empty($imgHref)) return false;
		$imgHref = $this->domen . '/' . $imgHref;	
		
		$imgName = $this->getRand().substr($imgHref,strrpos($imgHref,'/')+1);
		
		if($copyImg) $this->imageresize('../data/imgs/'.$imgName,$imgHref,600,600,75); //ограничиваю размер не более 600px и качество 75
		
		return $imgName;
		//file_put_contents('../data/imgs/'.substr($url,strrpos($url,'/')+1), file_get_contents($url));
	}
    
    private function imageresize($outfile,$infile,$neww,$newh,$quality) {
		$im=imagecreatefromjpeg($infile);
		$k1=$neww/imagesx($im);
		$k2=$newh/imagesy($im);
		$k=$k1>$k2?$k2:$k1;

		$w=intval(imagesx($im)*$k);
		$h=intval(imagesy($im)*$k);

		$im1=imagecreatetruecolor($w,$h);
		imagecopyresampled($im1,$im,0,0,0,0,$w,$h,imagesx($im),imagesy($im));

		imagejpeg($im1,$outfile,$quality);
		imagedestroy($im);
		imagedestroy($im1);
    }
	public function getRand($prefix = '', $randNumber=false){
		if(!$randNumber) $randNumber=rand(0,10000);
		return $prefix.time().'_'.$randNumber;
	}
}

class VirtueMartParser extends Parser{
	
	/*pages_array
		[0]=>array(
			'название поля'=>'значение поля',
			...
			'название поля'=>'значение поля',
			'если картинка'=>array('ссылка на картинку','ссылка на картинку'...)
		),...
	*/
	public function createGoodsXMLNew($pages_array){
		
		$dom = new domDocument("1.0", "utf-8"); // Создаём XML-документ версии 1.0 с кодировкой utf-8
		$root = $dom->createElement("goods"); // Создаём корневой элемент	
		$dom->appendChild($root);
		
		foreach($pages_array as $page){
			$dom = $this->insertPageIntoDOM($dom, $page);
		}
		
		$dom->save($this->dataFileUrl); // Сохраняем полученный XML-документ в файл
	}
	
	private function insertPageIntoDOM($dom, $page_arr){
		$new_field = '';
		
		foreach ($page_arr['file_urls'] as $fu){
			$product = $dom->createElement("product");
			
			foreach ($page_arr as $fieldKey=>$fieldValue){
				if($fieldKey=='file_urls') continue;
				$new_field = $dom->createElement($fieldKey,$fieldValue);
				$product->appendChild($new_field);
			}
			$file_url = $dom->createElement("file_url", $fu);
			$product->appendChild($file_url);
			
			$dom->documentElement->appendChild($product);
		}
		
		$product = $dom->createElement("product");
		foreach ($page_arr as $fieldKey=>$fieldValue){
			if($fieldKey=='file_urls')continue;
			$new_field = $dom->createElement($fieldKey,$fieldValue);
			$product->appendChild($new_field);
		}
		$file_url = $dom->createElement("file_url", '');
		$product->appendChild($file_url);
		$dom->documentElement->appendChild($product);
		
		return $dom;
	}
	
	public function removeNullАrticles(){
		try {
			$dbh = new PDO('mysql:host=localhost;dbname=telfs_7slonov', 'telfs_7slonov', '7slonov7');
			$dbh->query('UPDATE `7slonov_virtuemart_products` SET `product_sku`="" WHERE `product_sku` LIKE "no_sku%"');
		$dbh = null;
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
	}
	
	public function getParentIds($mid,$cid){
		try {
			$sth = $this->dbh->prepare("SELECT pr.virtuemart_product_id, pr.product_name from `7slonov_virtuemart_products_ru_ru` as pr 
						LEFT JOIN `7slonov_virtuemart_product_manufacturers` as pm ON pr.virtuemart_product_id=pm.virtuemart_product_id 
						LEFT JOIN `7slonov_virtuemart_product_categories` as pc ON pr.virtuemart_product_id=pc.virtuemart_product_id
						WHERE pm.virtuemart_manufacturer_id=$mid
						AND pc.virtuemart_category_id=$cid");
			$sth->execute();

			$result = $sth->fetchAll();
			return $result;
		} catch (PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
	}
}

class OpenCartParser extends Parser{
    
    public function getFeaturesString($featuresArr,$delimiter='|'){
        $str = '';
        if(empty($featuresArr))return false;
        foreach($featuresArr as $feature){
            $str .= 'Характеристики'.$delimiter.$feature['f_key'].$delimiter.$feature['f_val'].'\n\r';
        }
        if(empty($str)) return false;
        return $str;
    }
}